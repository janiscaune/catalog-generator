CSV GENERATOR FOR MAGENTO CATALOG

HOW IT WORKS:

1) This generator can be used to create a CSV file that is later passed to Magmi and used to create new products in Magento.

2) This means that you will also need Magmi with Image processor plugin installed and configured. Best option is to create own custom profile for 
this purpose. 

3) This generator can create CSV for simple products and configurable products. Names and descriptions are created randomly,
random images are picked up from data_source/images/ folder.


HOW TO USE IT:

1) You need to have a Magento installation with following features:

- at least one configurable product attribute that can be used to create configurables
  (default for this - "size", values - 35..45).

- some categories created. You can adjust the category sets in data_source/categories.txt 
  (each line represents a set of category IDs this product belongs to).

2) You also need to have Magmi installed:

- enable the Configurable Item Processor and select "Perform simples/configurable link" and "auto match simples skus before configurable".
- install Image attributes processor and point image search path to the path of this readme
- save this as a new profile (don't forget to save DB/user settings etc.) and insert profile name in catalogenerator.rb
- also, include full path to Magmi CLI executable into catalogenerator.rb

3) Remaining generator configuration:
- add images in data_source/images folder
- set desired amount of simple and configurable products

4) Running the generator

- From shell - [ruby executable] catalogenerator.rb
- If there will be any errors, you will be notified.

TROUBLESHOOTING & DISCLAIMER

1) This has only been tested (so far) with default Magento CE 1.7.0.2, Magmi 0.7.18, and Ruby 1.9.2. 
2) If you want to have specific feature added (like add some attribute to import) - I'm pretty sure you will make it.
3) If you want to add specific names or description sentences - just add them.

And most importantly - this code has been created with the intention "to just make it work" - it is not supposed to be unbreakable and universal.
Comments and questions welcome - janisc@majaslapa.lv
