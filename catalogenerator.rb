# This is the main catalog generator file
# First - define required files, classes and filename variables
require './lib/file_utils.rb'
require './lib/product_data_utils.rb'

$file_utils = FileUtils.new
$product_utils = ProductDataUtils.new

# You might want to change these!
output_file_name = 'output.csv'
simple_count = 500
configurable_count = 100
SIMPLES_PER_CONFIGURABLE = 3 
CONFIGURABLE_ATTRIBUTE_NAME = 'size'
php_exec = 'php'
magmi_exec = '/var/www/supercatalog/magmi/cli/magmi.cli.php'
magmi_profile = 'test'
magmi_mode = 'create'

# First obtain lists of sentences, names and images
SENTENCE_ARRAY = $product_utils.get_sentence_array
NAME_ARRAY = $product_utils.get_name_array
IMAGE_FILE_ARRAY = $product_utils.get_image_array
CAT_ARRAY = $product_utils.get_category_array
COUNTRIES_ARRAY = $product_utils.get_country_array

# Then open output file and insert header
header = "sku,attribute_set,configurable_attributes,type,category_ids,country_of_manufacture,description,has_options,image,msrp_display_actual_price_type,msrp_enabled,name,price,required_options,short_description,size,small_image,status,tax_class_id,thumbnail,thumbnail_label,visibility,weight,qty,media_attribute_id,"
$file_utils.write_line(output_file_name, header)


# Configurables
configurable_count.times do |z|
        product_line = $product_utils.create_configurable
        $file_utils.write_line(output_file_name, product_line)
end

# Simples
simple_count.times do |z|
        product_line = $product_utils.create_simple
        $file_utils.write_line(output_file_name, product_line)
end



# Launching Magmi
`#{php_exec} #{magmi_exec} -profile=#{magmi_profile} -mode=#{magmi_mode} -CSV:filename="#{output_file_name}"`
# And removing generated CSV
`rm #{output_file_name}`

