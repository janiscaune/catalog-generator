class ProductDataUtils

    def get_sentence_array
        sentence_ary = $file_utils.get_list('data_source/sentences.txt')
        return sentence_ary
    end


    def get_name_array
        name_ary = $file_utils.get_list('data_source/names.txt')
        return name_ary
    end

    def get_category_array
        name_ary = $file_utils.get_list('data_source/categories.txt')
        return name_ary
    end


    def get_image_array
        image_ary = $file_utils.get_name_list('data_source/images/*')
        return image_ary
    end

    def get_country_array
        image_ary = $file_utils.get_list('data_source/countries.txt')
        return image_ary
    end

    def create_simple
        product = Hash.new
        product[:sku] = get_random_sku
        product[:name] = get_random_name
        product[:short_description] = get_sentences(3)
        product[:description] = get_sentences(7)
        product[:image_url] = get_image_url
        product[:prod_type] = "simple"
        product[:country] = get_random_country
        product[:categories] = get_random_category
        line = make_product_line(product)
        return line
    end

    def create_configurable
        # For Magmi to create a configurable product, a couple of
        # simples with SKU=(conf. prod. SKU)+something need to be before configurable product.
        # So first configurable attrs. will be added and then lines for simples
        # and finally line for configurable
        product = Hash.new
        product[:sku] = get_random_sku
        product[:name] = get_random_name
        product[:short_description] = get_sentences(3)
        product[:description] = get_sentences(7)
        product[:image_url] = get_image_url
        product[:prod_type] = "simple"
        product[:country] = get_random_country
        product[:categories] = get_random_category
        return_string = ''
        SIMPLES_PER_CONFIGURABLE.times do |i|
            line = make_simple_product_line(product, i)
            return_string += line + "\n"
        end
        return_string += make_configurable_product_line(product)
        return return_string
    end

    def get_random_sku
        # Makes 8-character random SKU 
        return (0...8).map { (65 + rand(26)).chr }.join
    end

    def get_random_name
        total = NAME_ARRAY.length - 1
        name = ''
        3.times {|n| name += NAME_ARRAY[rand(total)].capitalize + ' '} 
        return name.gsub("\n","")
    end

    def get_sentences(cnt)
        total = SENTENCE_ARRAY.length - 1
        line = ''
        cnt.times {|n| line += SENTENCE_ARRAY[rand(total)] + ' '}
        return line.gsub("\n","")
    end

    def get_image_url
        total = IMAGE_FILE_ARRAY.length - 1
        return IMAGE_FILE_ARRAY[rand(total)].gsub("\n","")
    end

    def get_random_country
        total = COUNTRIES_ARRAY.length - 1
        return COUNTRIES_ARRAY[rand(total)].gsub("\n","")
    end

    def get_random_category
        total = CAT_ARRAY.length - 1
        return CAT_ARRAY[rand(total)].gsub("\n","")
    end

    def make_product_line(product)
        line = '"' + product[:sku] +'",Default,,simple,"' + product[:categories] + '","' + product[:country] +'","' + product[:description] + '",0,+' + product[:image_url] + ',Use config,Use config,"' + product[:name] +'",' + rand(1000).to_s + ',0,"' + product[:short_description] + '",' + rand(35..45).to_s + ',+' + product[:image_url] + ',1,2,+' + product[:image_url] + ',"label",4,1,43,88,'
        return line
    end

    def make_simple_product_line(product, numerator)
        line = '"' + product[:sku] + '-' + numerator.to_s + '",Default,' + CONFIGURABLE_ATTRIBUTE_NAME + ',simple,"' + product[:categories] + '","' + product[:country] +'","' + product[:description] + '",0,,Use config,Use config,"' + product[:name] +'",' + rand(1000).to_s + ',0,"' + product[:short_description] + '",' + (35 + numerator).to_s + ',,1,2,,,1,1,43,,'
        return line
    end

    def make_configurable_product_line(product)
        line = '"' + product[:sku] +'",Default,' + CONFIGURABLE_ATTRIBUTE_NAME + ',configurable,"' + product[:categories] + '","' + product[:country] +'","' + product[:description] + '",0,+' + product[:image_url] + ',Use config,Use config,"' + product[:name] +'",' + rand(1000).to_s + ',1,"' + product[:short_description] + '",' + rand(35..45).to_s + ',+' + product[:image_url] + ',1,2,+' + product[:image_url] + ',"label",4,1,43,88,'
        return line
    end

end
