class FileUtils

    def get_list(filename)
        lines = Array.new()
        file = File.open(filename, "r") do |f|
            f.each_line do |l|
                lines.push(l)
            end
        end
        return lines
    end

    def get_name_list(dirname)
        name_list = Array.new
        Dir[dirname].each {|f| name_list.push(String(f))}
        return name_list
    end

    def write_line(filename, line)
        File.open(filename, 'a') do |f|
            f.puts line
        end 
    end

end
